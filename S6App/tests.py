from django.test import TestCase, Client
from django.urls import resolve
from .views import landing
from .models import PostMood
from .forms import MoodForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

#Unittest
class S6UnitTest(TestCase):
	def setup(self):
		moods = PostMood(mood = 'im stressd')
		return moods

   #Test Views
	def test_landing_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
    
	def test_S6_using_landing_func(self):
		found = resolve('/')
		self.assertEqual(found.func, landing)
		
	def test_using_landing_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'landing.html')
	
	def test_mood_creation(self):
		my_mood = self.setup()
		self.assertTrue(isinstance(my_mood, PostMood))
		self.assertEqual(str(my_mood), "{}.{}".format(my_mood.id, my_mood.mood))

    #Test Forms
	def test_form_can_be_used(self):
		web = Client().post('/',
		data = {'mood' : 'im stressd'}
        )
		self.assertTrue(PostMood.objects.filter(mood = 'im stressd').exists())

#Functional test
class S6FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
		super(S6FunctionalTest, self).setUp()
		
	def tearDown(self):
		self.selenium.quit()
		super(S6FunctionalTest, self).tearDown()
		
	def test_input_mood(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(5)
		mood = selenium.find_element_by_id('id_mood')
		submit = selenium.find_element_by_id('submit')
	
		mood.send_keys('im stressd')
	
		submit.click()
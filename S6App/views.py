from django.shortcuts import render
from django.shortcuts import redirect
from .forms import MoodForm
from .models import PostMood

def landing(request):
    mood_form = MoodForm(request.POST or None)
    if request.method == "POST":
        if mood_form.is_valid():
            mood_form.save()
            return redirect('/')
    else:
        mood_form = MoodForm()
            
    posts = PostMood.objects.all()
	
    contexts = {
        'posts': posts,
        'mood_form': mood_form,
    }
    return render(request, 'landing.html', contexts)
 